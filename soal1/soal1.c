#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <json-c/json.h>
#include <string.h>
#include <dirent.h>
#include <time.h>

char *listWeapons[130];
char *listChars[48];

void downloadFile(char *down_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/wget", down_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("download file success\n");
  }
}

void unzipFile(char *unzip_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/unzip", unzip_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("unzip file success\n");
  }
}

void rmFile(char *rm_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/rm", rm_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("remove file success\n");
  }
}

void mkdirFile(char *mkdir_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/mkdir", mkdir_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("mkdir file success\n");
  }
}

void touchFile(char *touch_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/touch", touch_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("touch file success\n");
  }
}

void createNewDir()
{
  char *num;
  int gacha = 1;
  int progems = 79000;
  for (int i = progems; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 90 == 0)
      {
        char dirName[100];
        if (asprintf(&num, "%d", gacha) == -1)
        {
          perror("asprintf");
        }
        strcat(strcpy(dirName, "gacha_gacha/total_gacha_"), num);
        free(num);

        char *args[] = {"mkdir", dirName, NULL};
        mkdirFile(args);
      }
      gacha++;
    }
  }
}

void createNewTxt()
{
  char *num;
  int gacha = 1;
  int gacha_num[] = {90, 180, 270, 360, 450};
  int count = 0;
  int progems = 79000;
  for (int i = 79000; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 10 == 0 && count < 5)
      {
        char fileName[100];
        if (asprintf(&num, "%d", gacha_num[count]) == -1)
        {
          perror("asprintf");
        }
        strcat(strcpy(fileName, "gacha_gacha/total_gacha_"), num);
        strcat(fileName, "/");
        free(num);

        if (asprintf(&num, "%d", gacha) == -1)
        {
          perror("asprintf");
        }
        strcat(fileName, "_gacha_");
        strcat(fileName, num);
        strcat(fileName, ".txt");
        free(num);

        char *args[] = {"touch", fileName, NULL};
        touchFile(args);
      }

      if (gacha % 90 == 0 && gacha != 0)
        count++;
      gacha++;
    }
  }
}

void getListJsonFile(char dir_name[])
{
  struct dirent *de;
  DIR *dr = opendir(dir_name);

  if (dir_name == "characters")
  {
    int i = 0;
    while ((de = readdir(dr)) != NULL)
    {
      if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
      {
        listChars[i] = (char *)malloc(strlen(de->d_name) + 1);
        strncpy(listChars[i], de->d_name, strlen(de->d_name));
        i++;
      }
    }
    closedir(dr);
  }
  else if (dir_name == "weapons")
  {
    int i = 0;
    while ((de = readdir(dr)) != NULL)
    {
      if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
      {
        listWeapons[i] = (char *)malloc(strlen(de->d_name) + 1);
        strncpy(listWeapons[i], de->d_name, strlen(de->d_name));
        i++;
      }
    }
    closedir(dr);
  }
}

void addNewText(char textName[], int gachaDir, int gachaFile)
{
  FILE *fp;
  char *num;

  // create dirName
  char dirName[100];
  if (asprintf(&num, "%d", gachaDir) == -1)
  {
    perror("asprintf");
  }
  strcat(strcpy(dirName, "gacha_gacha/total_gacha_"), num);
  free(num);
  strcat(dirName, "/");
  strcat(dirName, "_gacha_");
  if (asprintf(&num, "%d", gachaFile) == -1)
  {
    perror("asprintf");
  }
  strcat(dirName, num);
  strcat(dirName, ".txt");
  free(num);
  fp = fopen(dirName, "ab");
  fputs(textName, fp);
  fputs("\n", fp);
  fclose(fp);
}

void createNewText(int gacha, char type[], struct json_object *rarity, struct json_object *name, int sisa, int gachaDir, int gachaFile)
{
  char textName[1024];
  char *num;

  if (asprintf(&num, "%d", gacha) == -1)
  {
    perror("asprintf");
  }
  strcat(strcpy(textName, ""), num);
  strcat(textName, "_");
  strcat(textName, type);
  strcat(textName, "_");
  free(num);

  if (asprintf(&num, "%d", json_object_get_int(rarity)) == -1)
  {
    perror("asprintf");
  }

  strcat(textName, num);
  strcat(textName, "_");
  strcat(textName, json_object_get_string(name));
  strcat(textName, "_");
  free(num);
  if (asprintf(&num, "%d", sisa) == -1)
  {
    perror("asprintf");
  }
  strcat(textName, num);
  free(num);

  addNewText(textName, gachaDir, gachaFile);
}

void jsonParse(int gacha, int sisa, char directory[], char jsonFile[], int gachaDir, int gachaFile)
{
  FILE *fp;
  char buffer[10000];
  struct json_object *parsed_json;
  struct json_object *name;
  struct json_object *rarity;

  // create fileName
  char fileName[1024];
  strcat(strcpy(fileName, ""), directory);
  strcat(fileName, "/");
  strcat(fileName, jsonFile);

  fp = fopen(fileName, "r");
  fread(buffer, 10000, 1, fp);
  fclose(fp);

  parsed_json = json_tokener_parse(buffer);

  json_object_object_get_ex(parsed_json, "name", &name);
  json_object_object_get_ex(parsed_json, "rarity", &rarity);

  createNewText(gacha, directory, rarity, name, sisa, gachaDir, gachaFile);
}

int randNum(int min, int max)
{
  return rand() % (max - min + 1) + min;
}

void getNewText()
{
  int progems = 79000;
  int gacha = 1;
  int gachaFile = 10;
  int gacha_num[] = {90, 180, 270, 360, 450};
  int count = 0;
  int num;
  for (int i = progems; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 2 != 0)
      {
        num = randNum(0, 47);
        jsonParse(gacha, progems, "characters", listChars[num], gacha_num[count], gachaFile);
      }
      else
      {
        num = randNum(0, 129);
        jsonParse(gacha, progems, "weapons", listWeapons[num], gacha_num[count], gachaFile);
      }
      if (gacha % 90 == 0 && gacha != 0 && count < 5)
        count++;
      if (gacha % 10 == 0 && gacha != 0)
        gachaFile += 10;
      gacha++;
    }
  }
}

int main()
{
  // POIN A
  char *down_args[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "char.zip", NULL};
  char *down_args2[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons.zip", NULL};
  char *unz_args[] = {"unzip", "-q", "char.zip", NULL};
  char *unz_args2[] = {"unzip", "-q", "weapons.zip", NULL};
  char *rm_args[] = {"rm", "char.zip", NULL};
  char *rm_args2[] = {"rm", "weapons.zip", NULL};
  char *mkdir_args[] = {"mkdir", "gacha_gacha", NULL};

  downloadFile(down_args);
  downloadFile(down_args2);
  unzipFile(unz_args);
  unzipFile(unz_args2);
  rmFile(rm_args);
  rmFile(rm_args2);
  mkdirFile(mkdir_args);

  // Create Random File
  createNewDir();
  createNewTxt();
  getListJsonFile("characters");
  getListJsonFile("weapons");
  getNewText();
}
