#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <stdbool.h>


void create_folder(pid_t child_id){
    int status;
    
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/gabrielsitanggang/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "/home/gabrielsitanggang/sisop/modul2/drakor.zip", NULL};
        execv("/bin/unzip", argv);
    }    
}

void delete_folder(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char current[PATH_MAX];
    int status;

    // get current location
    strcpy(current, cwd);

    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            // Construct new path from our base path
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if(strchr(dp->d_name, '.') == 0 && strcmp(dp->d_name, "soal2") != 0){            
                if(child_id == 0){
                    printf("%s\n", dp->d_name);

                    char *argv[] = {"rm", "-rf", dp->d_name, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
                child_id = fork();
            }
        }
    }
}