#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>
#include <sys/stat.h>

int main(){
    
    pid_t child_id;
    int status;

    // char *argv[] = {"rm","-r","animal", NULL};
    // execv("/bin/rm", argv);

    child_id = fork();

    if (child_id < 0) exit(EXIT_FAILURE); 
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "home/gabrielsitanggang/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    while ((wait(&status)) > 0);
    sleep(3);
    child_id = fork(); 
    if (child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", "home/gabrielsitanggang/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }

    while((wait(&status)) > 0);
    child_id = fork(); 
    if (child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0){
        char *argv[] = {"unzip","-d","modul2", "animal", NULL};
        execv("/bin/unzip", argv);
    }  
    while((wait(&status)) > 0);

    DIR *dp;
    struct dirent *ep;
    char kewan[100] = "home/gabrielsitanggang/modul2/animal";
    char banyu[100] = "home/gabrielsitanggang/modul2/air";
    char darat[100] = "home/gabrielsitanggang/modul2/darat";

    dp = opendir(kewan);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
            if(strstr(ep->d_name, "darat") != NULL){
                char src[200] = "home/gabrielsitanggang/modul2/animal/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"mv",src,darat, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
            }
            if(strstr(ep->d_name, "air") != NULL){
                char src[200] = "home/gabrielsitanggang/modul2/animal/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"mv",src,banyu, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
            }  
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE); 
    if(child_id == 0){
        char *argv[] = {"rm","-r","home/gabrielsitanggang/modul2/animal/", NULL};
        execv("/bin/rm", argv);
    }
    while((wait(&status)) > 0);

    dp = opendir(darat);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
            if(strstr(ep->d_name, "bird") != NULL){
                char src[100] = "home/gabrielsitanggang/modul2/darat/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"rm",src, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
            }
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE); 
    if(child_id == 0){
        char *argv[] = {"touch","home/gabrielsitanggang/modul2/air/list.txt", NULL};
        execv("/bin/touch", argv);
    }      
    while((wait(&status)) > 0);

    FILE *list;
    list = fopen("home/gabrielsitanggang/modul2/air/list.txt","w+");

    dp = opendir(banyu);

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strstr(ep->d_name,"air") != NULL){
                struct stat fs;
                int r;
                char filee[200] = "home/gabrielsitanggang/modul2/air/";
                strcat(filee,ep->d_name);

                r = stat(filee,&fs);
                if(r == -1)
                {
                    fprintf(stderr,"File error\n");
                    exit(1);
                }
                struct passwd *pw = getpwuid(fs.st_uid);
                char namafile[150]="";
                strcat(namafile,pw->pw_name);
                strcat(namafile,"_");
                if( fs.st_mode & S_IRUSR )  strcat(namafile,"r");
                if( fs.st_mode & S_IWUSR )  strcat(namafile,"w");
                if( fs.st_mode & S_IXUSR )  strcat(namafile,"x");
                strcat(namafile,"_");
                strcat(namafile,ep->d_name);
                fprintf(list,"%s\n",namafile);
            }            
        }
        (void) closedir(dp);
    } else perror ("Couldn't open the directory");
}
