# Kelompok C09
* 5025201165 - Gabriel Solomon Sitanggang
* 5025201102 - Arya Nur Razzaq
* 5025201020 - Muhammad Ferdian Iqbal
<br><br>

# Soal 1
## Persoalan
- Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut.
- Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut. Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA
- Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.
- Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis. Contoh : 157_characters_5_Albedo_53880
- Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44. Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)


  
## Penyelesaian
``` c++
void downloadFile(char *down_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/wget", down_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("download file success\n");
  }
}

void unzipFile(char *unzip_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/unzip", unzip_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("unzip file success\n");
  }
}

void rmFile(char *rm_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/rm", rm_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("remove file success\n");
  }
}

void mkdirFile(char *mkdir_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/mkdir", mkdir_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("mkdir file success\n");
  }
}

void touchFile(char *touch_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/touch", touch_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("touch file success\n");
  }
}
```

> Beberapa fungsi di atas digunakan untuk mendownload file, unzip file, remove file, membuat direktori baru, dan membuat file baru.

``` c++
void createNewDir()
{
  char *num;
  int gacha = 1;
  int progems = 79000;
  for (int i = progems; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 90 == 0)
      {
        char dirName[100];
        if (asprintf(&num, "%d", gacha) == -1)
        {
          perror("asprintf");
        }
        strcat(strcpy(dirName, "gacha_gacha/total_gacha_"), num);
        free(num);

        char *args[] = {"mkdir", dirName, NULL};
        mkdirFile(args);
      }
      gacha++;
    }
  }
}
```

> fungsi createNewDir digunakan untuk membuat beberapa folder yang fungsinya untuk menampung beberapa file .txt seperti yang diminta pada soal. Setiap **gacha** % 90 bernilai 0, direktori akan dibuat.

``` C++
void createNewTxt()
{
  char *num;
  int gacha = 1;
  int gacha_num[] = {90, 180, 270, 360, 450};
  int count = 0;
  int progems = 79000;
  for (int i = 79000; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 10 == 0 && count < 5)
      {
        char fileName[100];
        if (asprintf(&num, "%d", gacha_num[count]) == -1)
        {
          perror("asprintf");
        }
        strcat(strcpy(fileName, "gacha_gacha/total_gacha_"), num);
        strcat(fileName, "/");
        free(num);

        if (asprintf(&num, "%d", gacha) == -1)
        {
          perror("asprintf");
        }
        strcat(fileName, "_gacha_");
        strcat(fileName, num);
        strcat(fileName, ".txt");
        free(num);

        char *args[] = {"touch", fileName, NULL};
        touchFile(args);
      }

      if (gacha % 90 == 0 && gacha != 0)
        count++;
      gacha++;
    }
  }
}
```

> fungsi createNewTxt digunakan untuk membuat file txt baru di dalam folder yang telah dibuat oleh fungsi **createNewDir()**. setiap jumlah **gacha** % 10 bernilai 0, file txt akan dibuat dan ada sekitar 9 file di setiap foldernya.

``` C++
int randNum(int min, int max)
{
  return rand() % (max - min + 1) + min;
}

void getNewText()
{
  int progems = 79000;
  int gacha = 1;
  int gachaFile = 10;
  int gacha_num[] = {90, 180, 270, 360, 450};
  int count = 0;
  int num;
  for (int i = progems; i > 0; i -= 160)
  {
    progems -= 160;
    if (progems > 0)
    {
      if (gacha % 2 != 0)
      {
        num = randNum(0, 47);
        jsonParse(gacha, progems, "characters", listChars[num], gacha_num[count], gachaFile);
      }
      else
      {
        num = randNum(0, 129);
        jsonParse(gacha, progems, "weapons", listWeapons[num], gacha_num[count], gachaFile);
      }
      if (gacha % 90 == 0 && gacha != 0 && count < 5)
        count++;
      if (gacha % 10 == 0 && gacha != 0)
        gachaFile += 10;
      gacha++;
    }
  }
}
```

> fungsi randNum() digunakan untuk mendapatkan nilai random dari rentang angka tertentu. Fungsi tersebut digunakan pada fungsi getNewText(). Fungsi ini adalah fungsi utama yang nantinya akan memanggil fungsi - fungsi lain. Fungsi ini berguna untuk menentukan file apa yang akan diparsing dahulu. Jika jumlah gacha bernilai genap, maka akan diparsing file characters. Sedangkan untuk gacha bernilai ganjil akan diparsing file weapon. kemudian untuk parameter ketiga pada **jsonParse** digunakan untuk mengambil list random dari array listChars atau listWeapons yang berisi masing - masing file json di dalam folder characters dan weapons. Untuk parameter kelima berguna untuk menentukan direktori untuk meletakkan hasil json yang telah diparsing, begitu juga dengan gachaFile.

``` C++
void jsonParse(int gacha, int sisa, char directory[], char jsonFile[], int gachaDir, int gachaFile)
{
  FILE *fp;
  char buffer[10000];
  struct json_object *parsed_json;
  struct json_object *name;
  struct json_object *rarity;

  // create fileName
  char fileName[1024];
  strcat(strcpy(fileName, ""), directory);
  strcat(fileName, "/");
  strcat(fileName, jsonFile);

  fp = fopen(fileName, "r");
  fread(buffer, 10000, 1, fp);
  fclose(fp);

  parsed_json = json_tokener_parse(buffer);

  json_object_object_get_ex(parsed_json, "name", &name);
  json_object_object_get_ex(parsed_json, "rarity", &rarity);

  createNewText(gacha, directory, rarity, name, sisa, gachaDir, gachaFile);
}
```

> fungsi jsonParse digunakan untuk memparsing tiap - tiap file Json sesuai dengan parameter yang diberikan. Hasilnya akan dimasukkan ke fungsi CreateNewText untuk dijadikan sebuah kalimat.

``` C++
void createNewText(int gacha, char type[], struct json_object *rarity, struct json_object *name, int sisa, int gachaDir, int gachaFile)
{
  char textName[1024];
  char *num;

  if (asprintf(&num, "%d", gacha) == -1)
  {
    perror("asprintf");
  }
  strcat(strcpy(textName, ""), num);
  strcat(textName, "_");
  strcat(textName, type);
  strcat(textName, "_");
  free(num);

  if (asprintf(&num, "%d", json_object_get_int(rarity)) == -1)
  {
    perror("asprintf");
  }

  strcat(textName, num);
  strcat(textName, "_");
  strcat(textName, json_object_get_string(name));
  strcat(textName, "_");
  free(num);
  if (asprintf(&num, "%d", sisa) == -1)
  {
    perror("asprintf");
  }
  strcat(textName, num);
  free(num);

  addNewText(textName, gachaDir, gachaFile);
}
```

> Fungsi createNewText merupakan kelanjutkan setelah file Json berhasil diparsing dan diambil nama dan rarity. Kemudian, fungsi createNewText akan membuat kalimat sesuai dengan perintah soal. Lalu, akan dituliskan ke file .txt yang sudah dibuat sebelumnya.

``` C++
void addNewText(char textName[], int gachaDir, int gachaFile)
{
  FILE *fp;
  char *num;

  // create dirName
  char dirName[100];
  if (asprintf(&num, "%d", gachaDir) == -1)
  {
    perror("asprintf");
  }
  strcat(strcpy(dirName, "gacha_gacha/total_gacha_"), num);
  free(num);
  strcat(dirName, "/");
  strcat(dirName, "_gacha_");
  if (asprintf(&num, "%d", gachaFile) == -1)
  {
    perror("asprintf");
  }
  strcat(dirName, num);
  strcat(dirName, ".txt");
  free(num);
  fp = fopen(dirName, "ab");
  fputs(textName, fp);
  fputs("\n", fp);
  fclose(fp);
}

```

> Fungsi addNewText digunakan untuk menuliskan kalimat yang dilemparkan dari fungsi createNewText ke dalam file .txt sesuai dengan lokasi yang diberikan.

``` C++
char *listWeapons[130];
char *listChars[48];

void getListJsonFile(char dir_name[])
{
  struct dirent *de;
  DIR *dr = opendir(dir_name);

  if (dir_name == "characters")
  {
    int i = 0;
    while ((de = readdir(dr)) != NULL)
    {
      if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
      {
        listChars[i] = (char *)malloc(strlen(de->d_name) + 1);
        strncpy(listChars[i], de->d_name, strlen(de->d_name));
        i++;
      }
    }
    closedir(dr);
  }
  else if (dir_name == "weapons")
  {
    int i = 0;
    while ((de = readdir(dr)) != NULL)
    {
      if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
      {
        listWeapons[i] = (char *)malloc(strlen(de->d_name) + 1);
        strncpy(listWeapons[i], de->d_name, strlen(de->d_name));
        i++;
      }
    }
    closedir(dr);
  }
}
```

> Fungsi getListJsonFile digunakan untuk mendapatkan list file yang terdapat di direktori characters atau weapons. Hasilnya akan dimasukkan ke dalam array listWeapons atau listChars. Array ini digunakan untuk menentukan file mana yang akan diparse di fungsi getNewText.

``` C++
int main()
{
  // POIN A
  char *down_args[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "char.zip", NULL};
  char *down_args2[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons.zip", NULL};
  char *unz_args[] = {"unzip", "-q", "char.zip", NULL};
  char *unz_args2[] = {"unzip", "-q", "weapons.zip", NULL};
  char *rm_args[] = {"rm", "char.zip", NULL};
  char *rm_args2[] = {"rm", "weapons.zip", NULL};
  char *mkdir_args[] = {"mkdir", "gacha_gacha", NULL};

  downloadFile(down_args);
  downloadFile(down_args2);
  unzipFile(unz_args);
  unzipFile(unz_args2);
  rmFile(rm_args);
  rmFile(rm_args2);
  mkdirFile(mkdir_args);

  // Create Random File
  createNewDir();
  createNewTxt();
  getListJsonFile("characters");
  getListJsonFile("weapons");
  getNewText();
}
```

> Fungsi main berisi beberapa array args untuk mendownload, unzip, remove file dan membuat direktori baru. Kemudian, array - array tersebut akan dilempar ke beberapa fungsi yang telah dijelaskan sebagai argumen. Setelah itu, Direktori dan file txt akan dibuat untuk menampung hasil parsingan. ListJson akan diambil dan gacha akan mulai dilakukan.

# Soal 2
## Persoalan
- Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
- Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.
- Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama. Contoh: “/drakor/romance/start-up.png”.
- Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.
- Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).

## Penyelesaian
``` C
    void create_folder(pid_t child_id){
    int status;
    
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/gabrielsitanggang/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "/home/gabrielsitanggang/sisop/modul2/drakor.zip", NULL};
        execv("/bin/unzip", argv);
    }    
}

```

> Fungsi diatas berguna untuk mengekstrak file drakor.zip dengan unzip yang dieksekusi dengan execv

``` C
    void delete_folder(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char current[PATH_MAX];
    int status;

    // get current location
    strcpy(current, cwd);

    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            // Construct new path from our base path
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if(strchr(dp->d_name, '.') == 0 && strcmp(dp->d_name, "soal2") != 0){            
                if(child_id == 0){
                    printf("%s\n", dp->d_name);

                    char *argv[] = {"rm", "-rf", dp->d_name, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
                child_id = fork();
            }
        }
    }
}

```

> Fungsi diatas membaca file-file dalam directory dengan library dirent.h. Program akan mendapat lokasi folder dengan strcpy(current, cwd) dimana lokasi file ditampung dalam variabel current. Selanjutnya program akan menyeleksi file tanpa tanda titik (.) yang merupakan file folder, maka program akan mengeksekusi sytax rm -rf, berfungsi untuk menghapus folder tidak penting.


# Soal 3
## Persoalan
- Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 
- Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
- Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
- Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
- Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut. Contoh : conan_rwx_hewan.png

## Penyelesaian
``` C
    child_id = fork();

    if (child_id < 0) exit(EXIT_FAILURE); 
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "home/gabrielsitanggang/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    while ((wait(&status)) > 0);
    sleep(3);
    child_id = fork(); 
    if (child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", "home/gabrielsitanggang/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
```
> Pertama-tama membuat direktori bernama darat, kemudian selama 3 detik membuat direktori baru bernama air. Direktori darat dan air dibuat dalam /home/[USER]/modul2/. Tiap pembuatan direktori tersebut diberi jeda 3 detik

``` C
child_id = fork(); 
    if (child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0){
        char *argv[] = {"unzip","-d","modul2", "animal", NULL};
        execv("/bin/unzip", argv);
    }  
    while((wait(&status)) > 0);
```

> Fungsi diatas berguna untuk mengekstrak animal.zip yang telah tersedia ke direktori /home/[USER]/modul2/

``` C
    DIR *dp;
    struct dirent *ep;
    char kewan[100] = "home/gabrielsitanggang/modul2/animal";
    char banyu[100] = "home/gabrielsitanggang/modul2/air";
    char darat[100] = "home/gabrielsitanggang/modul2/darat";

    dp = opendir(kewan);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
            if(strstr(ep->d_name, "darat") != NULL){
                char src[200] = "home/gabrielsitanggang/modul2/animal/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"mv",src,darat, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
            }
            if(strstr(ep->d_name, "air") != NULL){
                char src[200] = "home/gabrielsitanggang/modul2/animal/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"mv",src,banyu, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
            }  
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE); 
    if(child_id == 0){
        char *argv[] = {"rm","-r","home/gabrielsitanggang/modul2/animal/", NULL};
        execv("/bin/rm", argv);
    }
    while((wait(&status)) > 0);
```

> Fungsi berfungsi untuk memisahkan hasil ekstrak ke folder darat dan air. Untuk hewan yang tidak memiliki keterangan darat maupun air, maka dihapus. Proses ini menggunakan dirent.h untuk mengakses nama setiap file

``` C
    dp = opendir(darat);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
            if(strstr(ep->d_name, "bird") != NULL){
                char src[100] = "home/gabrielsitanggang/modul2/darat/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"rm",src, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
            }
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
```

> Fungsi berfungsi untuk menghapus semua file yang memiliki keterangan bird di direktori darat.


``` C
    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE); 
    if(child_id == 0){
        char *argv[] = {"touch","home/gabrielsitanggang/modul2/air/list.txt", NULL};
        execv("/bin/touch", argv);
    }      
    while((wait(&status)) > 0);

    FILE *list;
    list = fopen("home/gabrielsitanggang/modul2/air/list.txt","w+");

    dp = opendir(banyu);

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strstr(ep->d_name,"air") != NULL){
                struct stat fs;
                int r;
                char filee[200] = "home/gabrielsitanggang/modul2/air/";
                strcat(filee,ep->d_name);

                r = stat(filee,&fs);
                if(r == -1)
                {
                    fprintf(stderr,"File error\n");
                    exit(1);
                }
                struct passwd *pw = getpwuid(fs.st_uid);
                char namafile[150]="";
                strcat(namafile,pw->pw_name);
                strcat(namafile,"_");
                if( fs.st_mode & S_IRUSR )  strcat(namafile,"r");
                if( fs.st_mode & S_IWUSR )  strcat(namafile,"w");
                if( fs.st_mode & S_IXUSR )  strcat(namafile,"x");
                strcat(namafile,"_");
                strcat(namafile,ep->d_name);
                fprintf(list,"%s\n",namafile);
            }            
        }
        (void) closedir(dp);
    } else perror ("Couldn't open the directory");
```

> Fungsi berfungsi untuk membuat file list.txt yang menampung seluruh data file file di direktori air dengan format UID_UID File Permission_File name.[jpg/png].